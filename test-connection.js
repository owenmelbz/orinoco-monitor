process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

const axios = require('axios');
const https = require('https');

const agent = new https.Agent({
    rejectUnauthorized: false,
});

axios.get('https://orinoco.stormdev.co.uk/dashboard', {
    httpsAgent: agent,
}).then(response => {
    console.log((response))
}).catch(error => {
    console.error((error))
});
