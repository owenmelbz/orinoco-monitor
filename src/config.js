const fs = require('fs');

const CUSTOM_CONFIG = __dirname + '/../paths.config.js';

if (!fs.existsSync(CUSTOM_CONFIG)) {
    console.log('Config does not exist - creating one.')
    fs.writeFileSync(CUSTOM_CONFIG, 'module.exports = [];')
}

const paths = require(CUSTOM_CONFIG);

const unique = array => {
    let a = array.concat();

    for (let i = 0; i < a.length; ++i) {
        for (let j = i + 1; j < a.length; ++j) {
            if (a[i] === a[j]) {
                a.splice(j--, 1);
            }
        }
    }

    return a;
}

const domain = process.env.ORINOCO_DOMAIN || 'orinoco.stormdev.co.uk';

module.exports = {
    paths: unique([
        '/var/log/nginx/*error.log',
        '/var/log/php*-fpm.log',
    ].concat(paths)),
    endpoint: `https://${domain}/api/logs/update`,
    key: process.env.ORINOCO_KEY,
}

console.log('Loaded domain: ' + domain)
console.log('Loaded key: ' + process.env.ORINOCO_KEY)
