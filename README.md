# Orinoco Monitor

This tool simply watches for changes within a directory via the `chokidar` package.

Upon detecting a change, it uploads the files to the main Orinoco database on `orinoco.stormdev.co.uk`.

## Installation

### Local Development

- `git clone` the repository
- `npm install` to install dependencies
- configure the `paths.config.js` to the directories you'd like to watch
- run `node index.js` to start the monitor.

## Remote deployment

> The Orinoco UI package comes with the installer and should be installed via that.

### Manual installation

- First do the same as you would for the local installation,
- Visit the "Daemons" section within the chosen server and set up a new daemon with...
 - Command `node index.js`
 - Directory: `/path-to-where-you-git-cloned-the-project`
 - User: `root`
 - Processes: `1`
 - Start Seconds: `1`
- Visit the Orinoco UI and trigger a 404 or error on the site to see it appear.